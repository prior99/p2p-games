import * as React from "react";
import { TSDI } from "tsdi";
import "./app.scss";
import { Background } from "./ui/background/background";
import { allAudios } from "./audio";
import { main, AppContextType, CommonUser, CommonUserState } from "p2p-games-common";
import { Game, SerializedGameState } from "./game";
import { GameConfig, MessageType, Phase } from "./types";
import Logo from "../assets/logo.png";
import { GameComponent } from "./ui";

declare const SOFTWARE_VERSION: string;

(async (): Promise<void> => {
    // Start dependency injection.
    const tsdi = new TSDI();
    tsdi.enableComponentScanner();

    const context: AppContextType<
        MessageType,
        Phase,
        CommonUser,
        GameConfig,
        CommonUserState,
        SerializedGameState,
        CommonUserState
    > = {
        game: tsdi.get(Game),
        identifier: "frabbage",
        name: "Frabbage",
        logoUrl: Logo,
        userProps: {},
        version: SOFTWARE_VERSION,
    };
    main({
        audios: allAudios,
        background: () => Background,
        gameComponent: <GameComponent />,
        context,
        tsdi,
    });
})();
