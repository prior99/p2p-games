import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { Phase } from "../../types";
import { Game } from "../../game";
import { unreachable, CommonPhase } from "p2p-games-common";
import { GamePhaseLobby } from "../game-phase-lobby";
import { GamePhaseWriteQuestion } from "../game-phase-write-question";
import { GamePhaseWriteAnswers } from "../game-phase-write-answers";
import { GamePhaseSelectAnswer } from "../game-phase-select-answer";
import { GamePhaseReveal } from "../game-phase-reveal";
import { GamePhaseScores } from "../game-phase-scores";

@external
@observer
export class GameComponent extends React.Component {
    @inject private game!: Game;

    public render(): JSX.Element {
        switch (this.game.phase) {
            case CommonPhase.LOBBY:
                return <GamePhaseLobby />;
            case Phase.WRITE_QUESTION:
                return <GamePhaseWriteQuestion />;
            case Phase.WRITE_ANSWERS:
                return <GamePhaseWriteAnswers />;
            case Phase.SELECT_ANSWER:
                return <GamePhaseSelectAnswer />;
            case Phase.REVEAL:
                return <GamePhaseReveal />;
            case Phase.SCORES:
                return <GamePhaseScores />;
            default:
                unreachable(this.game.phase);
        }
    }
}
