import { create as randomSeed } from "random-seed";
import { MessageFactory } from "p2p-networking";
import { computed, action, observable } from "mobx";
import { component } from "tsdi";
import {
    GameConfig,
    Phase,
    AppUser,
    MessageType,
    MessageQuestion,
    MessageAnswer,
    MessageSelectAnswer,
    MessageDoneReading,
    MessageNextRound,
    MessageSetCorrectAnswerOutcome,
} from "./types";
import { v4 } from "uuid";
import { audioAnswerAddOther, audioAnswerAddSelf, audioQuestionDone, audioAcceptWaiting } from "./audio";
import { AbstractGame, CommonUser, CommonMessageType, shuffle, CommonUserState } from "p2p-games-common";

declare const SOFTWARE_VERSION: string;

export interface Answer {
    answerId: string;
    title: string;
    userId: string;
}

export const enum CorrectAnswerOutcome {
    CORRECT = "correct",
    WRONG = "wrong",
}

export interface SerializedGameState {
    round: number;
    turnOrder: string[];
    answers: [string, Answer][];
    selectedAnswers: [string, string][];
    correctAnswers: [string, string][];
    correctAnswerOutcomes: [string, CorrectAnswerOutcome][];
    doneReading: string[];
    question: string | undefined;
}

@component
export class Game extends AbstractGame<
    MessageType,
    Phase,
    CommonUser,
    GameConfig,
    CommonUserState,
    SerializedGameState,
    CommonUserState
> {
    @observable public round = 0;
    @observable public turnOrder: string[] = [];
    @observable public answers = new Map<string, Answer>();
    @observable public selectedAnswers = new Map<string, string>();
    @observable public doneReading = new Set<string>();
    @observable public question?: string;
    @observable public correctAnswers = new Map<string, string>();
    @observable public correctAnswerOutcomes = new Map<string, CorrectAnswerOutcome>();

    private messageQuestion?: MessageFactory<MessageType | CommonMessageType, MessageQuestion>;
    private messageAnswer?: MessageFactory<MessageType | CommonMessageType, MessageAnswer>;
    private messageSelectAnswer?: MessageFactory<MessageType | CommonMessageType, MessageSelectAnswer>;
    private messageDoneReading?: MessageFactory<MessageType | CommonMessageType, MessageDoneReading>;
    private messageNextRound?: MessageFactory<MessageType | CommonMessageType, MessageNextRound>;
    private messageCorrectAnswerOutcome?: MessageFactory<
        MessageType | CommonMessageType,
        MessageSetCorrectAnswerOutcome
    >;

    constructor() {
        super({ seed: v4() }, SOFTWARE_VERSION);
    }

    @computed public get questionMaster(): AppUser | undefined {
        return this.userList.find((user) => this.isUserQuestionMaster(user.id));
    }

    public async sendNextRound(): Promise<void> {
        await this.sendMessage(this.messageNextRound, { config: this.config });
    }

    public async sendCorrectAnswerOutcome(userId: string, outcome: CorrectAnswerOutcome): Promise<void> {
        await this.sendMessage(this.messageCorrectAnswerOutcome, { userId, outcome });
    }

    public async sendQuestion(question: string): Promise<void> {
        await this.sendMessage(this.messageQuestion, { question });
    }

    public async sendAnswer(title: string, correctAnswer: string | undefined): Promise<void> {
        await this.sendMessage(this.messageAnswer, { title, answerId: v4(), correctAnswer });
    }

    public async sendSelectAnswer(answerId: string): Promise<void> {
        await this.sendMessage(this.messageSelectAnswer, { answerId });
    }

    public async sendDoneReading(): Promise<void> {
        await this.sendMessage(this.messageDoneReading, {});
    }

    @computed public get canSelectAnswer(): boolean {
        return this.questionMaster?.id === this.userId && this.phase === Phase.SELECT_ANSWER;
    }

    @computed public get submittedAnswer(): Answer | undefined {
        return Array.from(this.answers.values()).find((modifier) => modifier.userId === this.userId);
    }

    public isUserQuestionMaster(userId: string): boolean {
        return this.turnOrder[this.round % this.turnOrder.length] === userId;
    }

    @computed public get isQuestionMaster(): boolean {
        return this.isUserQuestionMaster(this.userId);
    }

    public isUserRiddler(userId: string): boolean {
        return !this.isUserQuestionMaster(userId);
    }

    @computed public get isRiddler(): boolean {
        return this.isUserRiddler(this.userId);
    }

    @computed public get allRiddlers(): AppUser[] {
        return this.turnOrder.filter((id) => this.isUserRiddler(id)).map((id) => this.getUser(id)!);
    }

    @computed public get finishedAnswerUsers(): AppUser[] {
        const modifiers = Array.from(this.answers.values());
        return this.allRiddlers.filter((user) => modifiers.some((modifier) => modifier.userId === user.id));
    }

    @computed public get missingAnswerUsers(): AppUser[] {
        return this.userList.filter((user) => !this.finishedAnswerUsers.some((other) => other.id === user.id));
    }

    @action.bound private startTurn(): void {
        this.question = undefined;
        for (const { id } of this.userList) {
            const state = this.userStates.get(id);
            if (!state) {
                this.userStates.set(id, {
                    score: 0,
                });
            }
            this.selectedAnswers.clear();
            this.doneReading.clear();
            this.answers.clear();
            this.correctAnswers.clear();
            this.correctAnswerOutcomes.clear();
        }
        if (!this.peer) {
            throw new Error("Network not initialized.");
        }
        this.nextPhase(Phase.WRITE_QUESTION);
    }

    @action.bound private nextPhase(phase: Phase, mute = false): void {
        if (!mute) {
            this.audios.play(audioQuestionDone);
        }
        this.phase = phase;
    }

    private checkGoToReveal(): void {
        if (
            this.allRiddlers.every(({ id }) => this.selectedAnswers.has(id)) &&
            Array.from(this.correctAnswers.keys()).every((userId) => this.correctAnswerOutcomes.has(userId))
        ) {
            this.nextPhase(Phase.REVEAL);
        }
    }

    @action.bound protected applyCustomGameState({
        round,
        turnOrder,
        correctAnswerOutcomes,
        answers,
        selectedAnswers,
        correctAnswers,
        doneReading,
        question,
    }: SerializedGameState) {
        this.round = round;
        this.turnOrder = turnOrder;
        this.answers = new Map(answers);
        this.selectedAnswers = new Map(selectedAnswers);
        this.doneReading = new Set(doneReading);
        this.correctAnswers = new Map(correctAnswers);
        this.question = question;
        this.correctAnswerOutcomes = new Map(correctAnswerOutcomes);
    }

    protected get serializedCustomGameState(): SerializedGameState {
        return {
            answers: Array.from(this.answers.entries()),
            selectedAnswers: Array.from(this.selectedAnswers.entries()),
            turnOrder: this.turnOrder,
            round: this.round,
            doneReading: Array.from(this.doneReading.values()),
            question: this.question,
            correctAnswers: Array.from(this.correctAnswers),
            correctAnswerOutcomes: Array.from(this.correctAnswerOutcomes),
        };
    }

    protected serializeUserState(state: CommonUserState): CommonUserState {
        return state;
    }

    protected deserializeUserState(state: CommonUserState): CommonUserState {
        return state;
    }

    @action.bound public async initialize(userProps: {}, networkId?: string, userId?: string): Promise<void> {
        await super.initialize(userProps, networkId, userId);

        this.messageQuestion = this.peer?.message<MessageQuestion>(MessageType.QUESTION);
        this.messageAnswer = this.peer?.message<MessageAnswer>(MessageType.ANSWER);
        this.messageSelectAnswer = this.peer?.message<MessageSelectAnswer>(MessageType.SELECT_ANSWER);
        this.messageDoneReading = this.peer?.message<MessageDoneReading>(MessageType.DONE_READING);
        this.messageNextRound = this.peer?.message<MessageNextRound>(MessageType.NEXT_ROUND);
        this.messageCorrectAnswerOutcome = this.peer?.message<MessageSetCorrectAnswerOutcome>(
            MessageType.SET_CORRECT_ANSWER_OUTCOME,
        );

        this.messageCorrectAnswerOutcome?.subscribe(
            action(({ userId, outcome }) => {
                this.correctAnswerOutcomes.set(userId, outcome);
                this.checkGoToReveal();
            }),
        );
        this.messageAnswer?.subscribe(
            action(({ title, answerId, correctAnswer }, userId) => {
                this.answers.set(answerId, { title, answerId, userId });
                if (correctAnswer) {
                    this.correctAnswers.set(userId, correctAnswer);
                }
                if (
                    !this.userList.every(({ id }) =>
                        Array.from(this.answers.values()).some((answer) => answer.userId === id),
                    )
                ) {
                    if (this.userId === userId) {
                        this.audios.play(audioAnswerAddSelf);
                    } else {
                        this.audios.play(audioAnswerAddOther);
                    }
                    return;
                }
                this.nextPhase(Phase.SELECT_ANSWER);
            }),
        );
        this.messageQuestion?.subscribe(
            action(({ question }) => {
                this.question = question;
                this.audios.play(audioQuestionDone);
                this.nextPhase(Phase.WRITE_ANSWERS, true);
            }),
        );
        this.messageSelectAnswer?.subscribe(
            action(({ answerId }, userId) => {
                this.selectedAnswers.set(userId, answerId);
                if (this.userId === userId) {
                    this.audios.play(audioAnswerAddSelf);
                } else {
                    this.audios.play(audioAnswerAddOther);
                }
                this.checkGoToReveal();
            }),
        );
        this.messageDoneReading?.subscribe(
            action((_, userId) => {
                this.doneReading.add(userId);
                if (this.doneReading.size === this.allRiddlers.length - 1 && !this.doneReading.has(this.userId)) {
                    this.audios.play(audioAcceptWaiting);
                } else {
                    if (this.userId === userId) {
                        this.audios.play(audioAnswerAddSelf);
                    } else {
                        this.audios.play(audioAnswerAddOther);
                    }
                }
                if (this.userList.every(({ id }) => this.doneReading.has(id))) {
                    this.selectedAnswers.forEach((answerId, userId) => {
                        const answer = this.answers.get(answerId);
                        if (answer?.userId === this.questionMaster?.id) {
                            this.userStates.get(userId)!.score += 20;
                        } else {
                            this.userStates.get(answer!.userId)!.score += 10;
                        }
                    });
                    if (
                        !Array.from(this.selectedAnswers.values()).includes(
                            Array.from(this.answers.values()).find(
                                (answer) => this.questionMaster!.id === answer.userId,
                            )!.answerId,
                        )
                    ) {
                        this.userStates.get(this.questionMaster!.id)!.score += 20;
                    }
                    this.correctAnswerOutcomes.forEach((outcome, userId) => {
                        if (outcome === CorrectAnswerOutcome.CORRECT) {
                            this.userStates.get(userId)!.score += 20;
                        } else {
                            this.userStates.get(userId)!.score -= 20;
                        }
                    });
                    this.nextPhase(Phase.SCORES);
                }
            }),
        );
        this.messageStartGame?.subscribe(
            action(({ config }) => {
                this.audios.play(audioQuestionDone);
                const rng = randomSeed(config.seed);
                this.turnOrder = shuffle(
                    this.userList.map(({ id }) => id),
                    () => rng.floatBetween(0, 1),
                );
                this.startTurn();
            }),
        );
        this.messageNextRound?.subscribe(
            action(() => {
                this.round++;
                this.startTurn();
            }),
        );
    }
}
