import { CorrectAnswerOutcome } from "../game";

export const enum MessageType {
    QUESTION = "question",
    ANSWER = "answer",
    SELECT_ANSWER = "select answer",
    DONE_READING = "done reading",
    NEXT_ROUND = "next round",
    SET_CORRECT_ANSWER_OUTCOME = "set correct answer outcome",
}

export interface MessageSetCorrectAnswerOutcome {
    userId: string;
    outcome: CorrectAnswerOutcome;
}

export interface MessageQuestion {
    question: string;
}

export interface MessageAnswer {
    title: string;
    answerId: string;
    correctAnswer: string | undefined;
}

export interface MessageSelectAnswer {
    answerId: string;
}

export interface MessageDoneReading {}

export interface MessageNextRound {}
