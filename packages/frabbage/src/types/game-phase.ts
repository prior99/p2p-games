export const enum Phase {
    WRITE_QUESTION = "write question",
    WRITE_ANSWERS = "write answers",
    SELECT_ANSWER = "select answer",
    REVEAL = "reveal",
    SCORES = "scores",
}