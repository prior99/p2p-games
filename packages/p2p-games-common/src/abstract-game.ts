import { MessageFactory, PeerOptions, NetworkMode } from "p2p-networking";
import NomineLipsum from "nomine-lipsum";
import { ObservablePeer, createObservableClient, createObservableHost } from "p2p-networking-mobx";
import { computed, action, observable } from "mobx";
import { component, inject } from "tsdi";
import { Audios } from "./audio";
import {
    CommonMessageType,
    MessageWelcome,
    MessageChangeConfig,
    MessageStartGame,
    CommonUser,
    CommonPhase,
    CommonSerializedGameState,
} from "./types";

export interface CommonScore {
    rank: number;
    score: number;
    playerName: string;
    playerId: string;
}

export interface CommonUserState {
    score: number;
}

@component
export abstract class AbstractGame<
    TMessageType extends string | number,
    TPhase extends string | number,
    TUser extends CommonUser = CommonUser,
    TConfig = {},
    TUserState extends CommonUserState = CommonUserState,
    TSerializedGameState = {},
    TSerializedUserState = CommonUserState
> {
    @inject protected audios!: Audios;

    @observable.ref public peer: ObservablePeer<TUser, TMessageType | CommonMessageType> | undefined = undefined;
    @observable public config: TConfig;
    @observable public phase: TPhase | CommonPhase = CommonPhase.LOBBY;
    @observable public loading = new Set<TMessageType | CommonMessageType>();
    @observable public userStates = new Map<string, TUserState>();

    protected messageWelcome?: MessageFactory<TMessageType | CommonMessageType, MessageWelcome<TConfig>>;
    protected messageGameState?: MessageFactory<
        TMessageType | CommonMessageType,
        CommonSerializedGameState<TConfig, TPhase | CommonPhase, TSerializedUserState> & TSerializedGameState
    >;
    protected messageChangeConfig?: MessageFactory<TMessageType | CommonMessageType, MessageChangeConfig<TConfig>>;
    protected messageStartGame?: MessageFactory<TMessageType | CommonMessageType, MessageStartGame<TConfig>>;

    protected abstract serializedCustomGameState: TSerializedGameState;
    protected abstract applyCustomGameState(state: TSerializedGameState): void;
    protected abstract serializeUserState(userState: TUserState): TSerializedUserState;
    protected abstract deserializeUserState(serialized: TSerializedUserState): TUserState;

    constructor(initialConfig: TConfig, protected version: string) {
        this.config = initialConfig;
    }

    @computed public get userName(): string {
        console.log(this.peer?.disconnectedUsers);
        return this.user?.name ?? "";
    }

    @computed public get userId(): string {
        return this.peer?.userId ?? "";
    }

    @computed public get scoreList(): CommonScore[] {
        return Array.from(this.userStates.entries())
            .sort(([_idA, a], [_idB, b]) => b.score - a.score)
            .map(([playerId, { score }]) => ({
                playerId,
                playerName: this.getUser(playerId)?.name ?? "",
                score,
            }))
            .map(({ playerName, playerId, score }, index) => ({
                rank: index + 1,
                score,
                playerName,
                playerId,
            }));
    }

    @computed public get userList(): TUser[] {
        return this.peer?.users ?? [];
    }

    @computed public get user(): TUser | undefined {
        return this.getUser(this.userId);
    }

    public getUser(userId: string): TUser | undefined {
        return this.peer?.getUser(userId);
    }

    public getRank(playerId: string): number {
        return this.scoreList.find((entry) => entry.playerId === playerId)?.rank ?? 0;
    }

    public changeName(newName: string): void {
        this.peer?.updateUser({ name: newName } as Partial<TUser>);
    }

    public async withLoading<TResolve>(
        feature: TMessageType | CommonMessageType,
        fn: () => Promise<TResolve>,
    ): Promise<TResolve> {
        this.loading.add(feature);
        try {
            const result = await fn();
            return result;
        } finally {
            this.loading.delete(feature);
        }
    }

    public async sendMessage<TMessage>(
        factory: MessageFactory<TMessageType | CommonMessageType, TMessage> | undefined,
        ...args: Parameters<MessageFactory<TMessageType | CommonMessageType, TMessage>["send"]>
    ): Promise<void> {
        if (!factory) {
            throw new Error("Network not initialized.");
        }
        const message = factory.send(...args);
        await this.withLoading(message.message.messageType, () => message.waitForAll());
    }

    public async sendStartGame(): Promise<void> {
        await this.sendMessage(this.messageStartGame, { config: this.config });
    }

    public async sendChangeConfig(): Promise<void> {
        await this.sendMessage(this.messageChangeConfig, { config: this.config });
    }

    @computed public get serializedGameState(): CommonSerializedGameState<TConfig, TPhase | CommonPhase, TSerializedUserState> &
        TSerializedGameState {
        const commonState: CommonSerializedGameState<TConfig, TPhase | CommonPhase, TSerializedUserState> = {
            phase: this.phase,
            config: this.config,
            userStates: Array.from(this.userStates.entries()).map(([key, value]) => [
                key,
                this.serializeUserState(value),
            ]),
        };
        return {
            ...this.serializedCustomGameState,
            ...commonState,
        };
    }

    @action.bound public applyGameState(
        state: CommonSerializedGameState<TConfig, TPhase | CommonPhase, TSerializedUserState> & TSerializedGameState,
    ): void {
        this.phase = state.phase;
        this.userStates = new Map(state.userStates.map(([key, value]) => [key, this.deserializeUserState(value)]));
        this.config = state.config;
        this.applyCustomGameState(state);
    }

    @computed public get networkMode(): NetworkMode {
        return this.peer?.networkMode ?? NetworkMode.DISCONNECTED;
    }

    @action.bound public async initialize(
        userProps: Omit<TUser, "name" | "id">,
        networkId?: string,
        userId?: string,
    ): Promise<void> {
        const options: PeerOptions<TUser> = {
            applicationProtocolVersion: this.version,
            peerJsOptions: {
                host: "peerjs.92k.de",
                secure: true,
            },
            pingInterval: 4,
            timeout: 10,
        };
        const user = {
            name: NomineLipsum.full(),
            ...userProps,
        } as TUser;
        this.peer =
            typeof networkId === "string"
                ? await createObservableClient(options, networkId, userId ? userId : user)
                : await createObservableHost(options, user);

        this.messageWelcome = this.peer.message<MessageWelcome<TConfig>>(CommonMessageType.WELCOME);
        this.messageChangeConfig = this.peer.message<MessageChangeConfig<TConfig>>(CommonMessageType.CHANGE_CONFIG);
        this.messageStartGame = this.peer.message<MessageStartGame<TConfig>>(CommonMessageType.START_GAME);
        this.messageGameState = this.peer.message<
            TSerializedGameState & CommonSerializedGameState<TConfig, TPhase | CommonPhase, TSerializedUserState>
        >(CommonMessageType.GAME_STATE);

        this.messageWelcome.subscribe(action(({ config }) => (this.config = config)));
        this.messageChangeConfig.subscribe(action(({ config }) => (this.config = config)));
        this.messageStartGame.subscribe(
            action(({ config }) => {
                this.config = config;
                for (const [_userId, state] of this.userStates) {
                    state.score = 0;
                }
            }),
        );
        this.messageGameState.subscribe(action((state) => this.applyGameState(state)));

        this.peer.on("userconnect", (user) => {
            if (!this.peer?.isHost) {
                return;
            }
            this.messageWelcome?.send({ config: this.config }, user.id);
        });
        this.peer.on("userreconnect", (user) => {
            if (!this.peer?.isHost) {
                return;
            }
            this.messageGameState?.send(this.serializedGameState, user.id);
        });
    }
}
