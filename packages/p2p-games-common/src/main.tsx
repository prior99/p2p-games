import * as React from "react";
import * as ReactDOM from "react-dom";
import { TSDI } from "tsdi";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import "./factories";
import { routeGame, routeMainMenu } from "./ui";
import { AudioManager } from "./audio";
import { Settings } from "./settings";
import { AbstractGame, CommonUserState } from "./abstract-game";
import { CommonUser } from "./types";

export interface AppContextType<
    TMessageType extends string | number,
    TPhase extends string | number,
    TUser extends CommonUser,
    TConfig,
    TUserState extends CommonUserState,
    TSerializedGameState,
    TSerializedUserState
> {
    name: string;
    identifier: string;
    version: string;
    logoUrl: string;
    game: AbstractGame<TMessageType, TPhase, TUser, TConfig, TUserState, TSerializedGameState, TSerializedUserState>;
    userProps: Omit<TUser, "name" | "id">;
}

export const AppContext = React.createContext({
    name: "Unknown",
    identifier: "unknown",
    version: "unknown",
    logoUrl: "http://example.com",
});

export async function main<
    TMessageType extends string | number,
    TPhase extends string | number,
    TUser extends CommonUser,
    TConfig,
    TUserState extends CommonUserState,
    TSerializedGameState,
    TSerializedUserState
>({
    background,
    audios,
    context,
    tsdi,
    gameComponent,
}: {
    background: () => React.ComponentClass<{ className?: string }>;
    audios: { url: string; gain: number }[];
    context: AppContextType<
        TMessageType,
        TPhase,
        TUser,
        TConfig,
        TUserState,
        TSerializedGameState,
        TSerializedUserState
    >;
    tsdi: TSDI;
    gameComponent: JSX.Element;
}): Promise<void> {
    tsdi.get(Settings).initialize(context.identifier);

    // Check HTML.
    const root = document.getElementById("app");
    if (!root) {
        throw new Error("Missing root tag on HTML page.");
    }
    root.innerHTML = "Loading...";

    // Load sounds.
    await Promise.all(audios.map(({ url, gain }) => tsdi.get(AudioManager).load(url, gain)));

    const Background = background();

    ReactDOM.render(
        <AppContext.Provider value={{ ...context }}>
            <div className="App">
                <Background className="App__background" />
                <div className="App__main">
                    <Router history={tsdi.get("history")}>
                        <Switch>
                            <Redirect exact from="/" to={routeMainMenu.path()} />
                            <Route path={routeMainMenu.pattern} component={routeMainMenu.component} />
                            <Route
                                path={routeGame.pattern}
                                render={(props) => (
                                    <routeGame.component {...props}>{gameComponent}</routeGame.component>
                                )}
                            />
                        </Switch>
                    </Router>
                </div>
            </div>
        </AppContext.Provider>,
        root,
    );
}
