export const enum CommonMessageType {
    WELCOME = "welcome",
    CHANGE_CONFIG = "change config",
    START_GAME = "start game",
    GAME_STATE = "game state",
}

export interface CommonSerializedGameState<TConfig, TPhase extends string | number, TUserState> {
    config: TConfig;
    phase: TPhase;
    userStates: [string, TUserState][];
}

export interface MessageWelcome<TConfig> {
    config: TConfig;
}

export interface MessageChangeConfig<TConfig> {
    config: TConfig;
}

export interface MessageStartGame<TConfig> {
    config: TConfig;
}
