import { User } from "p2p-networking";

export interface CommonUser extends User {
    name: string;
}