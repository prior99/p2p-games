import { component } from "tsdi";
import { observable } from "mobx";

const localStorageVersion = 1;

export interface SettingsValues {
    volume: number;
    version: number;
}

@component
export class Settings {
    @observable private values!: SettingsValues;
    private identifier = "p2p-games";

    constructor() {
        this.reset();
    }

    public initialize(identifier: string): void {
        this.identifier = identifier;
        const json = localStorage.getItem(this.identifier);
        if (!json) { return; }
        try {
            const settings: SettingsValues = JSON.parse(json);
            if (settings.version !== localStorageVersion) {
                console.error("Outdated settings encountered.");
                localStorage.removeItem(this.identifier);
            } else {
                this.values = settings
            }
        } catch (err) {
            console.error(`Failed to parse settings: ${json}`);
            localStorage.removeItem(this.identifier);
        }
    }

    public reset(): void {
        this.values = { volume: 0.3, version: localStorageVersion };
    }

    public get volume(): number {
        return this.values.volume;
    }

    public set volume(volume: number) {
        this.values.volume = volume;
        this.save();
    }

    private save(): void {
        const { volume } = this.values;
        const json = JSON.stringify({
            volume,
            version: localStorageVersion,
        });
        localStorage.setItem(this.identifier, json);
    }
}
