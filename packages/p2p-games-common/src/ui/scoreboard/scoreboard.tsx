import * as React from "react";
import { external } from "tsdi";
import { observer } from "mobx-react";
import { Table, Icon } from "semantic-ui-react";
import classNames from "classnames";
import { computed } from "mobx";
import "./scoreboard.scss";
import { UserTable } from "p2p-networking-semantic-ui-react";
import { AppContext, AppContextType } from "../../main";

export interface ScoreboardProps {
    className?: string;
}

@external
@observer
export class Scoreboard extends React.Component<ScoreboardProps> {
    public static contextType = AppContext;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public context!: AppContextType<any, any, any, any, any, any, any>;

    @computed private get classNames(): string {
        return classNames("Scoreboard", this.props.className);
    }

    public render(): JSX.Element {
        return (
            <UserTable
                headerCells={() => (
                    <>
                        <Table.HeaderCell className="Scoreboard__rankHeader">
                            <Icon name="trophy" />
                        </Table.HeaderCell>
                        <Table.HeaderCell textAlign="right" className="Scoreboard__scoreHeader">
                            Score
                        </Table.HeaderCell>
                    </>
                )}
                customCells={(user) => (
                    <>
                        <Table.Cell className="ScoreboardRow__rank">{this.context.game.getRank(user.id)}</Table.Cell>
                        <Table.Cell textAlign="right" className="ScoreboardRow__score">
                            {(this.context.game.userStates.get(user.id)?.totalScore ?? 0).toLocaleString()}
                        </Table.Cell>
                    </>
                )}
                peer={this.context.game.peer!}
                nameFactory={(user) => user.name}
                unstackable
                className={this.classNames}
            />
        );
    }
}
