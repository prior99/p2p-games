import * as React from "react";
import { addRoute, RouteProps } from "../../routing";
import { external } from "tsdi";
import { observer } from "mobx-react";
import { LobbyMode } from "../../types";
import "./page-game.scss";
import { ReconnectModal, ConnectLoader } from "p2p-networking-semantic-ui-react";
import { AppContext, AppContextType } from "../../main";
import { DisconnectedModal } from "../disconnected-modal";

export interface PageGameProps {
    lobbyMode: LobbyMode;
    peerId?: string;
    userId?: string;
}

@external
@observer
export class PageGame extends React.Component<RouteProps<PageGameProps>> {
    public static contextType = AppContext;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public context!: AppContextType<any, any, any, any, any, any, any>;

    async componentDidMount(): Promise<void> {
        const { lobbyMode, peerId, userId } = this.props.match.params;
        if (lobbyMode === LobbyMode.HOST) {
            await this.context.game.initialize(this.context.userProps);
        } else {
            if (userId) {
                await this.context.game.initialize(this.context.userProps, peerId!, userId);
            } else {
                await this.context.game.initialize(this.context.userProps, peerId!);
            }
        }
    }

    public render(): JSX.Element {
        return (
            <div className="PageGame">
                <DisconnectedModal />
                <ReconnectModal peer={this.context.game.peer} />
                <ConnectLoader peer={this.context.game.peer} />
                {this.props.children}
            </div>
        );
    }
}

export const routeGame = addRoute<PageGameProps>({
    path: (lobbyMode: LobbyMode, peerId?: string, userId?: string) => {
        switch (lobbyMode) {
            case LobbyMode.CLIENT:
                if (userId) {
                    return `/game/client/${peerId}/${userId}`;
                }
                return `/game/client/${peerId}`;
            case LobbyMode.HOST:
                return `/game/host`;
        }
    },
    pattern: "/game/:lobbyMode/:peerId?/:userId?",
    component: PageGame,
});
