import * as React from "react";
import { observer } from "mobx-react";
import { external } from "tsdi";
import { Dimmer, Loader } from "semantic-ui-react";
import { computed } from "mobx";
import { ReconnectMessage } from "p2p-networking-semantic-ui-react";
import { AppContext, AppContextType } from "../../main";
import { CommonUser } from "../../types";

export interface DisconnectedModalProps {
    className?: string;
}

@observer
@external
export class DisconnectedModal extends React.Component<DisconnectedModalProps> {
    public static contextType = AppContext;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public context!: AppContextType<any, any, any, any, any, any, any>;

    @computed private get disconnected(): CommonUser[] {
        return this.context.game.peer?.disconnectedUsers ?? [];
    }

    @computed private get active(): boolean {
        return Boolean(this.context.game.peer) && this.disconnected.length > 0;
    }

    public render(): JSX.Element {
        return (
            <Dimmer className={this.props.className} style={{ textAlign: "left" }} active={this.active}>
                {this.context.game.peer?.isHost ? (
                    this.disconnected.map((user) => (
                        <ReconnectMessage
                            style={{ width: 800 }}
                            peer={this.context.game.peer}
                            key={user.id}
                            nameFactory={(user: CommonUser) => user.name}
                            urlFactory={(peerId, user) =>
                                location.href.replace(location.hash, `#/game/client/${peerId}/${user.id}`)
                            }
                            userId={user.id}
                        />
                    ))
                ) : (
                    <Loader>Waiting for other user...</Loader>
                )}
            </Dimmer>
        );
    }
}
