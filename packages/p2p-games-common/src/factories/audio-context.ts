import { component, factory, initialize } from "tsdi";

@component
export class FactoryAudioContext {
    private audioContext!: AudioContext;

    @initialize
    public async initialize(): Promise<void> {
        this.audioContext = new (window.AudioContext || (window as any).webkitAudioContext)(); // eslint-disable-line
    }

    @factory({ name: "AudioContext" })
    public getAudioContext(): AudioContext {
        return this.audioContext;
    }
}
