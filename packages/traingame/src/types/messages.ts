export enum Track {
    TRACK_A = "track a",
    TRACK_B = "track b",
}

export enum CardType {
    GOOD = "good",
    BAD = "bad",
}

export const enum MessageType {
    CARD_ADD = "card add",
    CARD_RESCUE = "card rescue",
    CARD_SWAP = "card swap",
    CARD_KILL = "card kill",
    CARD_ADD_MODIFIER = "card add modifier",
    DECIDE = "decide",
    NEXT_ROUND = "next round"
}

export interface MessageCardAdd {
    title: string;
    cardId: string;
    cardType: CardType;
}

export interface MessageCardRescue {
    cardId: string;
}

export interface MessageCardSwap {
    cardIds?: [string, string];
}

export interface MessageCardKill {
    cardId: string;
}

export interface MessageCardAddModifier {
    cardId: string;
    modifierId: string;
    title: string;
}

export interface MessageDecide {
    track: Track;
}

export interface MessageNextRound {
}