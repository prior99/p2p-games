import { create as randomSeed } from "random-seed";
import { MessageFactory } from "p2p-networking";
import { computed, action, observable } from "mobx";
import { component } from "tsdi";
import {
    GameConfig,
    Phase,
    MessageType,
    MessageCardRescue,
    MessageCardSwap,
    MessageCardKill,
    MessageCardAddModifier,
    MessageDecide,
    MessageNextRound,
    CardType,
    MessageCardAdd,
    Track,
} from "./types";
import { v4 } from "uuid";
import {
    audioStart,
    audioAudioAddModifier,
    audioNextPhase,
    audioDecisionMade,
    audioRescue,
    audioKilled,
    audioSubmitGood,
    audioSubmitBad,
} from "./audio";
import { shuffle, AbstractGame, CommonUser, CommonMessageType } from "p2p-games-common";

declare const SOFTWARE_VERSION: string;

export interface UserState {
    score: number;
}

export interface Card {
    cardId: string;
    title: string;
    cardType: CardType;
    userId: string;
    track: Track;
    removed: boolean;
}

export interface Modifier {
    modifierId: string;
    cardId: string;
    userId: string;
    title: string;
}

export interface SerializedGameState {
    round: number;
    turnOrder: string[];
    cards: [string, Card][];
    modifiers: [string, Modifier][];
    swapped: string[];
    decidedTrack: Track | undefined;
}

@component
export class Game extends AbstractGame<
    MessageType,
    Phase,
    CommonUser,
    GameConfig,
    UserState,
    SerializedGameState,
    UserState
> {
    @observable public round = 0;
    @observable public turnOrder: string[] = [];
    @observable public cards = new Map<string, Card>();
    @observable public modifiers = new Map<string, Modifier>();
    @observable public swapped = new Set<string>();
    @observable public decidedTrack: Track | undefined;

    private messageCardAdd?: MessageFactory<MessageType | CommonMessageType, MessageCardAdd>;
    private messageCardRescue?: MessageFactory<MessageType | CommonMessageType, MessageCardRescue>;
    private messageCardSwap?: MessageFactory<MessageType | CommonMessageType, MessageCardSwap>;
    private messageCardKill?: MessageFactory<MessageType | CommonMessageType, MessageCardKill>;
    private messageCardAddModifier?: MessageFactory<MessageType | CommonMessageType, MessageCardAddModifier>;
    private messageDecide?: MessageFactory<MessageType | CommonMessageType, MessageDecide>;
    private messageNextRound?: MessageFactory<MessageType | CommonMessageType, MessageNextRound>;

    constructor() {
        super({ seed: v4() }, SOFTWARE_VERSION);
    }

    @computed public get canAddModifier(): boolean {
        return (
            this.phase === Phase.WRITE_MODIFIERS &&
            !Array.from(this.modifiers.values()).some((modifier) => modifier.userId === this.userId) &&
            !this.isConductor
        );
    }

    @computed public get conductor(): CommonUser | undefined {
        return this.userList.find((user) => this.isUserConductor(user.id));
    }

    public async sendNextRound(): Promise<void> {
        await this.sendMessage(this.messageNextRound, { config: this.config });
    }

    public async sendCardRescue(cardId: string): Promise<void> {
        await this.sendMessage(this.messageCardRescue, { cardId });
    }

    public async sendCardSwap(cardIds?: [string, string]): Promise<void> {
        await this.sendMessage(this.messageCardSwap, { cardIds });
    }

    public async sendCardAddModifier(title: string, cardId: string): Promise<void> {
        await this.sendMessage(this.messageCardAddModifier, { modifierId: v4(), title, cardId });
    }

    public async sendCardAdd(title: string, cardType: CardType): Promise<void> {
        await this.sendMessage(this.messageCardAdd, { cardId: v4(), title, cardType });
    }

    public async sendCardKill(cardId: string): Promise<void> {
        await this.sendMessage(this.messageCardKill, { cardId });
    }

    public async sendDecide(track: Track): Promise<void> {
        await this.sendMessage(this.messageDecide, { track });
    }

    @computed public get canSwap(): boolean {
        return this.swappingUser?.id === this.userId && this.phase === Phase.SWAP_CARDS;
    }

    @computed public get submittedModifier(): Modifier | undefined {
        return Array.from(this.modifiers.values()).find((modifier) => modifier.userId === this.userId);
    }

    @computed public get submittedCard(): Card | undefined {
        const cardType = this.phase === Phase.WRITE_GOOD ? CardType.GOOD : CardType.BAD;
        return Array.from(this.cards.values()).find(
            (card) => card.cardType === cardType && card.userId === this.userId,
        );
    }

    @computed public get swappingUser(): CommonUser | undefined {
        return this.allManiacs.find((user) => !this.swapped.has(user.id));
    }

    public isUserConductor(userId: string): boolean {
        return this.turnOrder[this.round % this.turnOrder.length] === userId;
    }

    @computed public get isConductor(): boolean {
        return this.isUserConductor(this.userId);
    }

    public cardsOnTrack(track: Track): Card[] {
        return Array.from(this.cards.values())
            .filter((card) => card.track === track)
            .sort((a, b) => {
                if (a.cardType === b.cardType) {
                    return a.cardId.localeCompare(b.cardId);
                }
                if (a.cardType === CardType.GOOD) {
                    return -1;
                }
                return 1;
            });
    }

    public isUserManiac(userId: string): boolean {
        return !this.isUserConductor(userId);
    }

    @computed public get isManiac(): boolean {
        return this.isUserManiac(this.userId);
    }

    @computed public get allManiacs(): CommonUser[] {
        return this.turnOrder.filter((id) => !this.isUserConductor(id)).map((id) => this.getUser(id)!);
    }

    @action.bound private handleKill(card: Card): void {
        const userState = this.userStates.get(card.userId);
        if (!userState) {
            throw new Error(`Unknown user: ${card.userId}`);
        }
        if (card.cardType === CardType.GOOD) {
            userState.score -= 10;
        }
        if (card.cardType === CardType.BAD) {
            userState.score += 20;
        }
    }

    @action.bound private handleRescue(card: Card): void {
        const userState = this.userStates.get(card.userId);
        if (!userState) {
            throw new Error(`Unknown user: ${card.userId}`);
        }
        if (card.cardType === CardType.GOOD) {
            userState.score += 50;
        }
        if (card.cardType === CardType.BAD) {
            userState.score -= 10;
        }
    }

    public getUserDefaultTrack(userId: string): Track {
        return [Track.TRACK_A, Track.TRACK_B][this.turnOrder.indexOf(userId) % 2];
    }

    @computed public get finishedModifierUsers(): CommonUser[] {
        const modifiers = Array.from(this.modifiers.values());
        return this.allManiacs.filter((user) => modifiers.some((modifier) => modifier.userId === user.id));
    }

    @computed public get finishedCardUsers(): CommonUser[] {
        const cards = Array.from(this.cards.values());
        if (this.phase === Phase.WRITE_BAD) {
            return this.allManiacs.filter((user) =>
                cards.some((card) => card.cardType === CardType.BAD && card.userId === user.id),
            );
        }
        return this.allManiacs.filter((user) =>
            cards.some((card) => card.cardType === CardType.GOOD && card.userId === user.id),
        );
    }

    @computed public get missingModifierUsers(): CommonUser[] {
        return this.allManiacs.filter((user) => !this.finishedModifierUsers.some((other) => other.id === user.id));
    }

    @computed public get missingCardUsers(): CommonUser[] {
        return this.allManiacs.filter((user) => !this.finishedCardUsers.some((other) => other.id === user.id));
    }

    @action.bound private startTurn(): void {
        this.decidedTrack = undefined;
        for (const { id } of this.userList) {
            const state = this.userStates.get(id);
            if (!state) {
                this.userStates.set(id, {
                    score: 0,
                });
            }
            this.cards.clear();
            this.modifiers.clear();
            this.swapped.clear();
        }
        if (!this.peer) {
            throw new Error("Network not initialized.");
        }
        this.nextPhase(Phase.WRITE_GOOD);
    }

    public getOpposingTrack(userId: string): Track {
        const existingCard = Array.from(this.cards.values()).find((card) => card.userId === userId);
        if (!existingCard) {
            throw new Error(`Missing card for user ${userId}`);
        }
        return existingCard.track === Track.TRACK_A ? Track.TRACK_B : Track.TRACK_A;
    }

    @action.bound private nextPhase(phase: Phase, mute = false): void {
        if (!mute) {
            this.audios.play(audioNextPhase);
        }
        this.phase = phase;
    }

    @action.bound public rearrangeTracks(): void {
        const cards = Array.from(this.cards.values())
            .filter((card) => !card.removed)
            .sort((a, b) => a.cardId.localeCompare(b.cardId));
        for (let index = 0; index < cards.length; ++index) {
            const track = index % 2 === 0 ? Track.TRACK_A : Track.TRACK_B;
            cards[index].track = track;
        }
    }

    @action.bound protected applyCustomGameState({
        cards,
        modifiers,
        turnOrder,
        round,
        swapped,
        decidedTrack,
    }: SerializedGameState): void {
        this.cards = new Map(cards);
        this.modifiers = new Map(modifiers);
        this.turnOrder = turnOrder;
        this.round = round;
        this.swapped = new Set(swapped);
        this.decidedTrack = decidedTrack;
    }

    protected get serializedCustomGameState(): SerializedGameState {
        return {
            cards: Array.from(this.cards.entries()),
            modifiers: Array.from(this.modifiers.entries()),
            turnOrder: this.turnOrder,
            round: this.round,
            swapped: Array.from(this.swapped.values()),
            decidedTrack: this.decidedTrack,
        };
    }

    protected serializeUserState(state: UserState): UserState {
        return state;
    }

    protected deserializeUserState(state: UserState): UserState {
        return state;
    }

    @action.bound public async initialize(userProps: {}, networkId?: string, userId?: string): Promise<void> {
        await super.initialize(userProps, networkId, userId);

        this.messageCardAdd = this.peer?.message<MessageCardAdd>(MessageType.CARD_ADD);
        this.messageCardRescue = this.peer?.message<MessageCardRescue>(MessageType.CARD_RESCUE);
        this.messageCardSwap = this.peer?.message<MessageCardSwap>(MessageType.CARD_SWAP);
        this.messageCardKill = this.peer?.message<MessageCardKill>(MessageType.CARD_KILL);
        this.messageCardAddModifier = this.peer?.message<MessageCardAddModifier>(MessageType.CARD_ADD_MODIFIER);
        this.messageDecide = this.peer?.message<MessageDecide>(MessageType.DECIDE);
        this.messageNextRound = this.peer?.message<MessageNextRound>(MessageType.NEXT_ROUND);

        this.messageCardAdd?.subscribe(
            action(({ title, cardId, cardType }, userId) => {
                const track =
                    cardType === CardType.GOOD ? this.getUserDefaultTrack(userId) : this.getOpposingTrack(userId);
                this.cards.set(cardId, {
                    title,
                    cardId,
                    userId,
                    cardType,
                    track,
                    removed: false,
                });
                if (
                    Array.from(this.cards.values()).filter((card) => {
                        if (this.phase === Phase.WRITE_BAD) {
                            return card.cardType === CardType.BAD;
                        }
                        return card.cardType === CardType.GOOD;
                    }).length < this.allManiacs.length
                ) {
                    if (cardType) {
                        this.audios.play(audioSubmitGood);
                    } else {
                        this.audios.play(audioSubmitBad);
                    }
                    return;
                }
                if (this.phase === Phase.WRITE_GOOD) {
                    this.rearrangeTracks();
                    if (this.allManiacs.length % 2 !== 0) {
                        this.nextPhase(Phase.RESCUE);
                    } else {
                        if (this.allManiacs.length >= 4) {
                            this.nextPhase(Phase.SWAP_CARDS);
                        } else {
                            this.nextPhase(Phase.WRITE_BAD);
                        }
                    }
                } else if (this.phase === Phase.WRITE_BAD) {
                    if (this.allManiacs.length % 2 !== 0) {
                        this.nextPhase(Phase.KILL);
                    } else {
                        this.nextPhase(Phase.WRITE_MODIFIERS);
                    }
                }
            }),
        );
        this.messageCardRescue?.subscribe(
            action(({ cardId }) => {
                const card = this.cards.get(cardId);
                if (!card) {
                    throw new Error(`Unknown card: ${cardId}`);
                }
                this.handleRescue(card);
                card.removed = true;
                this.rearrangeTracks();
                this.audios.play(audioRescue);
                if (this.allManiacs.length >= 4) {
                    this.nextPhase(Phase.SWAP_CARDS, true);
                } else {
                    this.nextPhase(Phase.WRITE_BAD, true);
                }
            }),
        );
        this.messageCardSwap?.subscribe(
            action(({ cardIds }, userId) => {
                if (cardIds) {
                    cardIds.forEach((cardId) => {
                        const card = this.cards.get(cardId);
                        if (!card) {
                            throw new Error(`Unknown card: ${cardId}`);
                        }
                        card.track = card.track === Track.TRACK_A ? Track.TRACK_B : Track.TRACK_A;
                    });
                }
                this.audios.play(audioAudioAddModifier);
                this.swapped.add(userId);
                if (this.allManiacs.every(({ id }) => this.swapped.has(id))) {
                    this.nextPhase(Phase.WRITE_BAD);
                }
            }),
        );
        this.messageCardKill?.subscribe(
            action(({ cardId }) => {
                const card = this.cards.get(cardId);
                if (!card) {
                    throw new Error(`Unknown card: ${cardId}`);
                }
                this.handleKill(card);
                card.removed = true;
                this.audios.play(audioKilled);
                this.nextPhase(Phase.WRITE_MODIFIERS, true);
            }),
        );
        this.messageCardAddModifier?.subscribe(
            action(({ cardId, modifierId, title }, userId) => {
                this.modifiers.set(modifierId, { title, cardId, userId, modifierId });
                if (this.modifiers.size === this.allManiacs.length) {
                    this.nextPhase(Phase.DECISION);
                } else {
                    this.audios.play(audioAudioAddModifier);
                }
            }),
        );
        this.messageDecide?.subscribe(
            action(({ track }) => {
                this.decidedTrack = track;
                Array.from(this.cards.values())
                    .filter((card) => !card.removed)
                    .forEach((card) => {
                        if (card.track === track) {
                            this.handleKill(card);
                        } else {
                            this.handleRescue(card);
                        }
                    });
                this.audios.play(audioDecisionMade);
                setTimeout(() => this.nextPhase(Phase.SCORES), 3000);
            }),
        );
        this.messageStartGame?.subscribe(
            action(({ config }) => {
                this.audios.play(audioStart);
                const rng = randomSeed(config.seed);
                this.turnOrder = shuffle(
                    this.userList.map(({ id }) => id),
                    () => rng.floatBetween(0, 1),
                );
                this.startTurn();
            }),
        );
        this.messageNextRound?.subscribe(
            action(() => {
                this.round++;
                this.startTurn();
            }),
        );
    }
}
