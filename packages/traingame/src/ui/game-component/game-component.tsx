import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { Phase } from "../../types";
import { Game } from "../../game";
import { unreachable, CommonPhase } from "p2p-games-common";
import { GamePhaseLobby } from "../game-phase-lobby";
import { GamePhaseScores } from "../game-phase-scores";
import { GamePhaseWriteCards } from "../game-phase-write-cards";
import { GamePhaseRescueKill } from "../game-phase-rescue-kill";
import { GamePhaseSwapCards } from "../game-phase-swap-cards";
import { GamePhaseWriteModifiers } from "../game-phase-write-modifiers";
import { GamePhaseDecision } from "../game-phase-decision";

@external
@observer
export class GameComponent extends React.Component {
    @inject private game!: Game;

    public render(): JSX.Element {
        switch (this.game.phase) {
            case CommonPhase.LOBBY:
                return <GamePhaseLobby />;
            case Phase.WRITE_GOOD:
                return <GamePhaseWriteCards />;
            case Phase.RESCUE:
                return <GamePhaseRescueKill />;
            case Phase.SWAP_CARDS:
                return <GamePhaseSwapCards />;
            case Phase.WRITE_BAD:
                return <GamePhaseWriteCards />;
            case Phase.KILL:
                return <GamePhaseRescueKill />;
            case Phase.WRITE_MODIFIERS:
                return <GamePhaseWriteModifiers />;
            case Phase.DECISION:
                return <GamePhaseDecision />;
            case Phase.SCORES:
                return <GamePhaseScores />;
            default:
                unreachable(this.game.phase);
        }
    }
}
