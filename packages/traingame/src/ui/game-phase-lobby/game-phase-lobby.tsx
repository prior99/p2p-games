import * as React from "react";
import { external } from "tsdi";
import { observer } from "mobx-react";
import { audioHover } from "../../audio";
import { Lobby } from "p2p-games-common";

declare const SOFTWARE_VERSION: string;

export interface GamePhaseLobbyProps {
    className?: string;
}

@external
@observer
export class GamePhaseLobby extends React.Component<GamePhaseLobbyProps> {
    public render(): JSX.Element {
        return <Lobby audio={audioHover} />
    }
}
