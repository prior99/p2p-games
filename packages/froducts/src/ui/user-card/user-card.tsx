import * as React from "react";
import classNames from "classnames";
import { observer } from "mobx-react";
import { external, inject } from "tsdi";
import { Game } from "../../game";
import "./user-card.scss";
import { CommonUser } from "p2p-games-common/src";
import { computed } from "mobx";

export interface UserCardProps {
    id: string;
    className?: string;
}

@external
@observer
export class UserCard extends React.Component<UserCardProps> {
    @inject private game!: Game;

    @computed private get user(): CommonUser | undefined {
        return this.game.getUser(this.props.id);
    }

    @computed private get price(): number | undefined {
        return this.game.userStates.get(this.props.id)?.price;
    }

    @computed private get score(): number | undefined {
        return this.game.userStates.get(this.props.id)?.score ?? 0;
    }

    @computed private get priceClass(): string {
        return classNames("UserCard__price", {
            "UserCard__price--empty": typeof this.price !== "number",
            "UserCard__price--done": typeof this.price === "number",
        });
    }
    @computed private get userClass(): string {
        return classNames("UserCard", {
            "UserCard--pending": typeof this.price !== "number",
            "UserCard--done": typeof this.price === "number",
        });
    }

    public render(): JSX.Element {
        return (
            <div className={this.userClass}>
                <div className="UserCard__details">
                    <div className="UserCard__name">{this.user?.name ?? ""}</div>
                    <div className={this.priceClass}>{this.price ?? "No clue"}</div>
                </div>
                <div className="UserCard__score">{this.score}</div>
            </div>
        );
    }
}
