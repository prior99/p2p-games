import * as React from "react";

export interface ProductFrameProps {
    html: string;
}

export default class ProductFrame extends React.Component<ProductFrameProps> {
    private writeHTML(frame: HTMLIFrameElement | null): void {
        const doc = frame?.contentDocument;
        if (!frame || !doc) {
            return;
        }

        doc.open();
        doc.write(this.props.html);
        doc.close();
        frame.style.width = "100%";
        frame.style.height = `${frame.contentWindow?.document.body.scrollHeight}px`;
    }

    public render(): JSX.Element {
        return <iframe src="about:blank" scrolling="no" frameBorder="0" ref={frame => this.writeHTML(frame)}></iframe>;
    }
}
