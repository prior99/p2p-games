import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { Game } from "../../game";
import { CommonPhase, unreachable } from "p2p-games-common";
import { GamePhaseLobby } from "../game-phase-lobby";
import { Phase } from "../../types";
import { GamePhaseScores } from "../game-phase-scores";
import { GamePhaseProduct } from "../game-phase-product";

@external
@observer
export class GameComponent extends React.Component {
    @inject private game!: Game;

    public render(): JSX.Element {
        switch (this.game.phase) {
            case CommonPhase.LOBBY:
                return <GamePhaseLobby />;
            case Phase.SCORES:
                return <GamePhaseScores />
            case Phase.PRODUCT:
                return <GamePhaseProduct />
            default:
                unreachable(this.game.phase);
        }
    }
}
