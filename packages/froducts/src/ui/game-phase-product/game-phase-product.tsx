import * as React from "react";
import { observer } from "mobx-react";
import { external, inject } from "tsdi";
import { Game } from "../../game";
import "./game-phase-product.scss";
import { Dimmer, Loader, Form, Segment } from "semantic-ui-react";
import ProductFrame from "../product-frame/product-frame";
import { UserCard } from "../user-card";
import { action, observable, computed } from "mobx";
import { MessageType } from "../../types";

@external
@observer
export class GamePhaseProduct extends React.Component {
    @inject private game!: Game;

    @observable private price?: string;

    @action.bound private async handleSubmit(): Promise<void> {
        await this.game.sendSubmitPrice(Number(this.price));
        this.price = undefined;
    }

    @action.bound private handlePriceChange(evt: React.SyntheticEvent<HTMLInputElement>): void {
        this.price = evt.currentTarget.value;
    }

    @computed private get valid(): boolean {
        return this.price !== undefined && this.price.length !== 0 && !isNaN(Number(this.price));
    }

    public render(): JSX.Element {
        return (
            <div className="GamePhaseProduct">
                <Dimmer active={!this.game.product}>
                    <Loader content="Loading product..." />
                </Dimmer>
                <div className="GamePhaseProduct__product">
                    {this.game.product && <ProductFrame html={this.game.product.html} />}
                </div>
                <Segment inverted className="GamePhaseProduct__bar">
                    <div className="GamePhaseProduct__form">
                        {typeof this.game.userStates.get(this.game.userId)?.price !== "number" && (
                            <Form onSubmit={this.handleSubmit} inverted>
                                <Form.Group inline>
                                    <Form.Field inverted>
                                        <label>Your guess</label>
                                        <Form.Input
                                            placeholder="10.00"
                                            onChange={this.handlePriceChange}
                                            label={{ basic: true, content: "€" }}
                                            labelPosition="right"
                                            inverted
                                        />
                                    </Form.Field>
                                    <Form.Field inverted>
                                        <label>Make guess</label>
                                        <Form.Button
                                            disabled={!this.valid || this.game.loading.has(MessageType.SUBMIT_PRICE)}
                                            loading={this.game.loading.has(MessageType.SUBMIT_PRICE)}
                                            inverted
                                            content="Okay"
                                            icon="idea"
                                        />
                                    </Form.Field>
                                </Form.Group>
                            </Form>
                        )}
                    </div>
                    <div className="GamePhaseProduct__users">
                        {this.game.userList.map(({ id }) => (
                            <UserCard key={id} id={id} className="GamePhaseProduct__user" />
                        ))}
                    </div>
                </Segment>
            </div>
        );
    }
}
