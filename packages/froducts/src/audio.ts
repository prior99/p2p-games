import audioHover from "../assets/sound-hover.mp3";

export {
    audioHover,
};

export const allAudios = [
    { url: audioHover, gain: 0.5 },
];
