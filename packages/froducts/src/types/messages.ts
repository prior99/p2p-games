export const enum MessageType {
    SUBMIT_PRICE = "submit price",
    NEXT_PRODUCT = "next product",
}

export interface MessageSubmitPrice {
    price: number;
}

export interface MessageNextProduct {
}