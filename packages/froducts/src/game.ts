import { create as randomSeed, RandomSeed } from "random-seed";
import { MessageFactory } from "p2p-networking";
import { computed, action, observable } from "mobx";
import { component } from "tsdi";
import { GameConfig, Phase, MessageType, MessageNextProduct, MessageSubmitPrice } from "./types";
import { v4 } from "uuid";
import { shuffle, AbstractGame, CommonUser, CommonMessageType } from "p2p-games-common";

declare const SOFTWARE_VERSION: string;

export interface UserState {
    score: number;
    price: number;
}

export interface Modifier {
    modifierId: string;
    cardId: string;
    userId: string;
    title: string;
}

export interface SerializedGameState {
    round: number;
}

export interface Product {
    price: number;
    html: string;
}

@component
export class Game extends AbstractGame<
    MessageType,
    Phase,
    CommonUser,
    GameConfig,
    UserState,
    SerializedGameState,
    UserState
> {
    @observable public round = 0;
    @observable public product?: Product;
    private rng?: RandomSeed;

    private messageNextProduct?: MessageFactory<MessageType | CommonMessageType, MessageNextProduct>;
    private messageSubmitPrice?: MessageFactory<MessageType | CommonMessageType, MessageSubmitPrice>;

    constructor() {
        super({ seed: v4() }, SOFTWARE_VERSION);
    }

    public async sendNextProduct(): Promise<void> {
        await this.sendMessage(this.messageNextProduct, { config: this.config });
    }

    public async sendSubmitPrice(price: number): Promise<void> {
        await this.sendMessage(this.messageSubmitPrice, { price });
    }

    @action.bound protected async loadProduct(url: string): Promise<void> {
        this.product = undefined;
        const res = await fetch(url);
        this.product = await res.json();
    }

    @action.bound private startTurn(): void {
        this.phase = Phase.PRODUCT;
        this.loadProduct(this.generateRandomUrl());
    }

    @action.bound protected applyCustomGameState({ round }: SerializedGameState): void {
        this.round = round;
        let url = "";
        for (let i = 0; i < round; ++i) {
            url = this.generateRandomUrl();
        }
        this.loadProduct(url);
    }

    private generateRandomUrl(): string {
        return `https://peerjs.92k.de/froducts/${this.rng?.intBetween(0, 10000000) ?? "unknown"}`;
        // return `http://localhost:9009/froducts/${this.rng? ?? "unknown"}`;
    }

    protected get serializedCustomGameState(): SerializedGameState {
        return {
            round: this.round,
        };
    }

    protected serializeUserState(state: UserState): UserState {
        return state;
    }

    protected deserializeUserState(state: UserState): UserState {
        return state;
    }

    @action.bound public async initialize(userProps: {}, networkId?: string, userId?: string): Promise<void> {
        await super.initialize(userProps, networkId, userId);

        this.messageNextProduct = this.peer?.message<MessageNextProduct>(MessageType.NEXT_PRODUCT);
        this.messageSubmitPrice = this.peer?.message<MessageSubmitPrice>(MessageType.SUBMIT_PRICE);

        this.messageStartGame?.subscribe(action(({ config }) => {
            this.rng = randomSeed(config.seed);
            this.startTurn();
        }));
        this.messageSubmitPrice?.subscribe(action(({ price }, userId) => {
            const state = this.userStates.get(userId);
            this.userStates.set(userId, {
                price: (state?.price ?? 0) + price,
                score:  (state?.score ?? 0),
            });
        }));
        this.messageNextProduct?.subscribe(
            action(() => {
                this.round++;
                this.startTurn();
            }),
        );
    }
}
