import * as React from "react";
import { TSDI } from "tsdi";
import "./app.scss";
import { Background } from "./ui/background/background";
import { allAudios } from "./audio";
import { main, AppContextType, CommonUser } from "p2p-games-common";
import { Game, SerializedGameState, UserState } from "./game";
import { GameConfig, Phase, MessageType } from "./types";
import { GameComponent } from "./ui";
import Logo from "../assets/logo.png";

declare const SOFTWARE_VERSION: string;

(async (): Promise<void> => {
    // Start dependency injection.
    const tsdi = new TSDI();
    tsdi.enableComponentScanner();

    const context: AppContextType<
        MessageType,
        Phase,
        CommonUser,
        GameConfig,
        UserState,
        SerializedGameState,
        UserState
    > = {
        game: tsdi.get(Game),
        identifier: "froducts",
        name: "Froducts",
        logoUrl: Logo,
        userProps: {},
        version: SOFTWARE_VERSION,
    };
    main({
        audios: allAudios,
        background: () => Background,
        gameComponent: <GameComponent />,
        context,
        tsdi,
    });
})();
