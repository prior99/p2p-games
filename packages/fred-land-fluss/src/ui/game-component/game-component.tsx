import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { Phase } from "../../types";
import { Game } from "../../game";
import { unreachable, CommonPhase } from "p2p-games-common";
import { GamePhaseLobby } from "../game-phase-lobby";
import { GamePhaseCountdown } from "../game-phase-countdown";
import { GamePhaseGuess } from "../game-phase-guess";
import { GamePhaseScores } from "../game-phase-scores";
import { GamePhaseScoring } from "../game-phase-scoring";

@external
@observer
export class GameComponent extends React.Component {
    @inject private game!: Game;

    public render(): JSX.Element {
        switch (this.game.phase) {
            case Phase.GUESS:
                if (this.game.inCountdown) {
                    return <GamePhaseCountdown />;
                } else {
                    return <GamePhaseGuess />;
                }
            case CommonPhase.LOBBY:
                return <GamePhaseLobby />;
            case Phase.SCORES:
                return <GamePhaseScores />;
            case Phase.SCORING:
                return <GamePhaseScoring />;
            default:
                unreachable(this.game.phase);
        }
    }
}
