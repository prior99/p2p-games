import * as React from "react";
import { external, inject } from "tsdi";
import { Segment, Form, Input, Grid, Button } from "semantic-ui-react";
import { computed, action, observable } from "mobx";
import { observer } from "mobx-react";
import { Game } from "../../game";
import { NetworkMode } from "p2p-networking";
import { audioTickOwn } from "../../audio";
import { Lobby } from "p2p-games-common";

declare const SOFTWARE_VERSION: string;

export interface GamePhaseLobbyProps {
    className?: string;
}

@external
@observer
export class GamePhaseLobby extends React.Component<GamePhaseLobbyProps> {
    @inject private game!: Game;

    @observable private focus = false;


    @computed private get isHost(): boolean {
        return this.game.peer?.networkMode === NetworkMode.HOST;
    }


    @action.bound private handleCategoryChange(value: string, index: number): void {
        this.focus = false;
        this.game.changeCategory(index, value);
    }

    @action.bound private handleCategoryDelete(index: number): void {
        this.game.deleteCategory(index);
    }

    @action.bound private handleCategoryAdd(value: string): void {
        this.focus = true;
        this.game.changeCategory(this.game.config.categories.length, value);
    }

    public render(): JSX.Element {
        return (
            <Lobby audio={audioTickOwn}>
                <Grid.Row>
                    {this.game.config.categories.map((category, index) => (
                        <Grid.Column key={index} computer="8" mobile="16">
                            {this.isHost ? (
                                <Form.Group inline>
                                    <Form.Field error={category.length === 0}>
                                        <label>Category {index + 1}</label>
                                        <Input
                                            ref={(input) =>
                                                this.focus &&
                                                index === this.game.config.categories.length - 1 &&
                                                input &&
                                                input.focus()
                                            }
                                            value={category}
                                            onChange={(evt) =>
                                                this.handleCategoryChange(evt.currentTarget.value, index)
                                            }
                                        />
                                    </Form.Field>
                                    <Form.Field>
                                        <label>Delete</label>
                                        <Button
                                            icon="trash"
                                            disabled={!this.isHost}
                                            onClick={() => this.handleCategoryDelete(index)}
                                        />
                                    </Form.Field>
                                </Form.Group>
                            ) : (
                                <Segment key={index} style={{ marginBottom: 10 }}>
                                    <Form.Field error={category.length === 0}>
                                        <label>Category {index + 1}</label>
                                        <p>{category}</p>
                                    </Form.Field>
                                </Segment>
                            )}
                        </Grid.Column>
                    ))}
                    {this.isHost && (
                        <Grid.Column key={this.game.config.categories.length} computer="8" mobile="16">
                            <Form.Group inline>
                                <Form.Field>
                                    <label>Category {this.game.config.categories.length + 1}</label>
                                    <Input
                                        value=""
                                        onChange={(evt) => this.handleCategoryAdd(evt.currentTarget.value)}
                                    />
                                </Form.Field>
                                <Form.Field>
                                    <label>Delete</label>
                                    <Button icon="trash" disabled />
                                </Form.Field>
                            </Form.Group>
                        </Grid.Column>
                    )}
                </Grid.Row>
            </Lobby>
        );
    }
}
