import { create as randomSeed, RandomSeed } from "random-seed";
import { MessageFactory } from "p2p-networking";
import { computed, action, observable } from "mobx";
import { component } from "tsdi";
import {
    GameConfig,
    Phase,
    MessageType,
    MessageNextRound,
    MessageEndRound,
    MessageSolution,
    MessageScoreWord,
    ScoreType,
    Validation,
    Letter,
    MessageTouchCategory,
    MessageSkipTurn,
    MessageAcceptScoring,
} from "./types";
import { v4 } from "uuid";
import { allLetters } from "./utils";
import {
    audioPass,
    audioUnpass,
    audioCountdown,
    audioTickOwn,
    audioTickOtherPlayer,
    audioFinishedFirst,
    audioFinishedOther,
    audioAcceptWaiting,
    audioAcceptSelf,
    audioAcceptOther,
    audioAcceptReset,
} from "./audio";
import { AbstractGame, CommonUser, CommonMessageType, CommonUserState } from "p2p-games-common";

declare const SOFTWARE_VERSION: string;

export interface SerializedUserState extends CommonUserState {
    solutions: [string, string][];
    currentScores: [string, ScoreType][];
    touchedCategories: string[];
    score: number;
    skipped: boolean;
    hasAcceptedScore: boolean;
}

export interface UserState extends CommonUserState {
    solutions: Map<string, string>;
    currentScores: Map<string, ScoreType>;
    touchedCategories: Set<string>;
    skipped: boolean;
    hasAcceptedScore: boolean;
}

export interface SerializedGameState {
    deadline: number;
    round: number;
    currentLetter: Letter;
    usedLetters: Letter[];
}

@component
export class Game extends AbstractGame<
    MessageType,
    Phase,
    CommonUser,
    GameConfig,
    UserState,
    SerializedGameState,
    SerializedUserState
> {
    @observable public deadline = Date.now();
    @observable public now = Date.now();
    @observable public round = 0;
    @observable public currentLetter = Letter.A;
    @observable public usedLetters = new Set<Letter>();

    private rng?: RandomSeed;

    private messageNextRound?: MessageFactory<MessageType | CommonMessageType, MessageNextRound>;
    private messageEndRound?: MessageFactory<MessageType | CommonMessageType, MessageEndRound>;
    private messageSolution?: MessageFactory<MessageType | CommonMessageType, MessageSolution>;
    private messageScoreWord?: MessageFactory<MessageType | CommonMessageType, MessageScoreWord>;
    private messageTouchCategory?: MessageFactory<MessageType | CommonMessageType, MessageTouchCategory>;
    private messageSkipTurn?: MessageFactory<MessageType | CommonMessageType, MessageSkipTurn>;
    private messageAcceptScoring?: MessageFactory<MessageType | CommonMessageType, MessageAcceptScoring>;

    constructor() {
        super(
            {
                seed: v4(),
                categories: ["Stadt", "Land", "Fluss"],
            },
            SOFTWARE_VERSION,
        );
    }

    @computed public get solution(): Map<string, string> | undefined {
        return this.userStates.get(this.userId)?.solutions;
    }

    public async sendNextRound(): Promise<void> {
        await this.sendMessage(this.messageNextRound, { config: this.config });
    }

    public async sendSkip(): Promise<void> {
        await this.sendMessage(this.messageSkipTurn, { skipped: !this.userStates.get(this.userId)?.skipped ?? true });
    }

    public async sendEndRound(): Promise<void> {
        await this.sendMessage(this.messageEndRound, { config: this.config });
    }

    public sendSolution(): void {
        this.sendMessage(this.messageSolution, { solution: Array.from(this.solution?.entries() ?? []) });
    }

    public sendTouchCategory(category: string): void {
        this.sendMessage(this.messageTouchCategory, { category });
    }

    public async sendScoreWord(userId: string, category: string, scoreType: ScoreType): Promise<void> {
        await this.sendMessage(this.messageScoreWord, { userId, category, scoreType });
    }

    public async sendAcceptScoring(): Promise<void> {
        await this.sendMessage(this.messageAcceptScoring, {});
    }

    public getScore(userId: string, category: string): ScoreType {
        return this.userStates.get(userId)?.currentScores.get(category) ?? ScoreType.NONE;
    }

    @action.bound public changeCategory(index: number, newName: string): void {
        this.config.categories[index] = newName;

        this.sendChangeConfig();
    }

    @action.bound public deleteCategory(index: number): void {
        this.config.categories.splice(index, 1);

        this.sendChangeConfig();
    }

    @computed public get validation(): Validation {
        const categoryErrors = new Map<string, string>();
        if (!this.solution) {
            throw new Error("Game not started.");
        }
        for (const [category, word] of this.solution) {
            if (word.trim() === "") {
                categoryErrors.set(category, "Word cannot be empty.");
                continue;
            }
            if (word.trim().toLowerCase()[0] !== this.currentLetter) {
                categoryErrors.set(category, "Wrong first letter.");
            }
        }
        if (categoryErrors.size === 0) {
            return { valid: true };
        }
        return { valid: false, categoryErrors };
    }

    @computed public get canEndTurn(): boolean {
        return this.validation.valid;
    }

    @computed public get availableLetters(): Set<Letter> {
        const result = new Set(allLetters);
        for (const letter of this.usedLetters) {
            result.delete(letter);
        }
        return result;
    }

    @computed public get allSkipped(): boolean {
        return this.userList.every(({ id }) => this.userStates.get(id)?.skipped);
    }

    @action.bound private generateLetter(): void {
        if (!this.rng) {
            return;
        }
        this.currentLetter = Array.from(this.availableLetters.values())[
            this.rng.intBetween(0, this.availableLetters.size - 1)
        ];
        this.usedLetters.add(this.currentLetter);
    }

    @action.bound private startTurn(): void {
        if (!this.rng) {
            throw new Error("Game not started.");
        }
        for (const { id } of this.userList) {
            const state = this.userStates.get(id);
            if (!state) {
                this.userStates.set(id, {
                    touchedCategories: new Set(),
                    currentScores: new Map(),
                    solutions: new Map(),
                    skipped: false,
                    score: 0,
                    hasAcceptedScore: false,
                });
            } else {
                state.hasAcceptedScore = false;
                state.touchedCategories.clear();
                state.currentScores.clear();
                state.solutions.clear();
                state.skipped = false;
            }
            for (const category of this.config.categories) {
                this.userStates.get(id)!.solutions.set(category, "");
            }
        }
        this.generateLetter();
        if (!this.peer) {
            throw new Error("Network not initialized.");
        }
        this.phase = Phase.GUESS;
        this.deadline = Date.now() + 8000;
        setTimeout(() => this.audios.play(audioCountdown), 1000);
        const interval = setInterval(() => {
            this.now = Date.now();
            if (!this.inCountdown) {
                clearInterval(interval);
            }
        }, 200);
    }

    public getWord(userId: string, category: string): string {
        return this.userStates.get(userId)?.solutions.get(category)?.toLowerCase() ?? "";
    }

    public getAllWords(category: string): Map<string, number> {
        const result = new Map<string, number>();
        for (const [_userId, state] of this.userStates) {
            const word = state.solutions.get(category) ?? "";
            const trimmed = word.trim().toLowerCase();
            if (!trimmed || trimmed.length === 0) {
                continue;
            }
            result.set(trimmed, (result.get(trimmed) ?? 0) + 1);
        }
        return result;
    }

    @action.bound private setScore(userId: string, category: string, score: ScoreType): void {
        this.userStates.get(userId)?.currentScores?.set(category, score);
    }

    private precalculateScores(): void {
        for (const category of this.config.categories) {
            const allWords = this.getAllWords(category);
            for (const { id: userId } of this.userList) {
                const word = this.getWord(userId, category);
                if (!word) {
                    this.setScore(userId, category, ScoreType.NONE);
                } else if (allWords.get(word) === 1) {
                    if (allWords.size === 1) {
                        this.setScore(userId, category, ScoreType.ONLY);
                    } else {
                        this.setScore(userId, category, ScoreType.UNIQUE);
                    }
                } else {
                    this.setScore(userId, category, ScoreType.DUPLICATE);
                }
            }
        }
    }

    private hasTouchedCategory(userId: string, category: string): boolean {
        return this.userStates.get(userId)?.touchedCategories.has(category) ?? false;
    }

    public getUntouched(category: string): number {
        let result = 0;
        for (const user of this.userList) {
            if (!this.hasTouchedCategory(user.id, category)) {
                result++;
            }
        }
        return result;
    }

    @action.bound public setWord(userId: string, category: string, word: string): void {
        this.userStates.get(userId)?.solutions.set(category, word);
        if (!this.hasTouchedCategory(userId, category)) {
            this.sendTouchCategory(category);
        }
    }

    @computed public get notAcceptedScoringCount(): number {
        return this.userList.reduce(
            (result, current) => (!this.userStates.get(current.id)!.hasAcceptedScore ? result + 1 : result),
            0,
        );
    }

    @computed public get inCountdown(): boolean {
        return this.now < this.deadline;
    }

    public isUserDone(userId: string): boolean {
        const state = this.userStates.get(userId)!;
        return state.skipped || this.config.categories.every((category) => Boolean(state.solutions.get(category)));
    }

    @action.bound private endRound(): void {
        this.phase = Phase.SCORING;
        this.sendSolution();
    }

    protected applyCustomGameState({ deadline, round, currentLetter, usedLetters }: SerializedGameState): void {
        this.deadline = deadline;
        this.round = round;
        this.rng = randomSeed(this.config.seed);
        while (this.currentLetter !== currentLetter) {
            this.generateLetter();
        }
        if (
            usedLetters.length !== this.usedLetters.size ||
            !usedLetters.every((letter) => this.usedLetters.has(letter))
        ) {
            throw new Error("Inconsistent random seed. Wrong sequence of letters generated.");
        }
    }

    protected get serializedCustomGameState(): SerializedGameState {
        return {
            deadline: this.deadline,
            round: this.round,
            currentLetter: this.currentLetter,
            usedLetters: Array.from(this.usedLetters.values()),
        };
    }

    protected serializeUserState({
        currentScores,
        hasAcceptedScore,
        score,
        skipped,
        solutions,
        touchedCategories,
    }: UserState): SerializedUserState {
        return {
            currentScores: Array.from(currentScores.entries()),
            score,
            skipped,
            solutions: Array.from(solutions.entries()),
            touchedCategories: Array.from(touchedCategories.values()),
            hasAcceptedScore,
        };
    }

    protected deserializeUserState({
        currentScores,
        hasAcceptedScore,
        score,
        skipped,
        solutions,
        touchedCategories,
    }: SerializedUserState): UserState {
        return {
            currentScores: new Map(currentScores),
            hasAcceptedScore,
            score,
            skipped,
            solutions: new Map(solutions),
            touchedCategories: new Set(touchedCategories),
        };
    }

    @action.bound public async initialize(userProps: {}, networkId?: string, userId?: string): Promise<void> {
        await super.initialize(userProps, networkId, userId);

        this.messageNextRound = this.peer?.message<MessageNextRound>(MessageType.NEXT_ROUND);
        this.messageEndRound = this.peer?.message<MessageEndRound>(MessageType.END_ROUND);
        this.messageSolution = this.peer?.message<MessageSolution>(MessageType.SOLUTION);
        this.messageScoreWord = this.peer?.message<MessageScoreWord>(MessageType.SCORE_WORD);
        this.messageTouchCategory = this.peer?.message<MessageTouchCategory>(MessageType.TOUCH_CATEGORY);
        this.messageSkipTurn = this.peer?.message<MessageSkipTurn>(MessageType.SKIP);
        this.messageAcceptScoring = this.peer?.message<MessageAcceptScoring>(MessageType.ACCEPT_SCORING);

        this.messageSkipTurn?.subscribe(
            action(({ skipped }, userId) => {
                if (skipped) {
                    this.audios.play(audioPass);
                } else {
                    this.audios.play(audioUnpass);
                }
                this.userStates.get(userId)!.skipped = skipped;
                if (this.allSkipped) {
                    this.endRound();
                }
            }),
        );

        this.messageTouchCategory?.subscribe(
            action(({ category }, userId) => {
                this.userStates.get(userId)?.touchedCategories.add(category);
                if (userId === this.userId) {
                    this.audios.play(audioTickOwn);
                } else {
                    this.audios.play(audioTickOtherPlayer);
                }
            }),
        );
        this.messageStartGame?.subscribe(
            action(({ config }) => {
                this.rng = randomSeed(config.seed);
                this.startTurn();
                for (const [_userId, state] of this.userStates) {
                    state.score = 0;
                }
            }),
        );
        this.messageNextRound?.subscribe(
            action(() => {
                this.round++;
                this.startTurn();
            }),
        );
        this.messageEndRound?.subscribe(
            action((_, userId) => {
                this.endRound();
                if (userId === this.userId) {
                    this.audios.play(audioFinishedFirst);
                } else {
                    this.audios.play(audioFinishedOther);
                }
            }),
        );
        this.messageSolution?.subscribe(
            action(({ solution }, userId) => {
                this.userStates.get(userId)!.solutions = new Map(solution);
                if (!this.userList) {
                    throw new Error("Network not initialized.");
                }
                this.precalculateScores();
            }),
        );
        this.messageScoreWord?.subscribe(
            action(({ userId, category, scoreType }) => {
                this.userStates.get(userId)?.currentScores.set(category, scoreType);
                this.audios.play(audioAcceptReset);
                for (const state of this.userStates.values()) {
                    state.hasAcceptedScore = false;
                }
            }),
        );
        this.messageAcceptScoring?.subscribe(
            action((_, userId) => {
                this.userStates.get(userId)!.hasAcceptedScore = true;
                const haveNotAccepted = this.userList.filter(({ id }) => !this.userStates.get(id)?.hasAcceptedScore);
                if (haveNotAccepted.length === 1 && haveNotAccepted[0].id === this.userId) {
                    this.audios.play(audioAcceptWaiting);
                } else {
                    if (userId === this.userId) {
                        this.audios.play(audioAcceptSelf);
                    } else {
                        this.audios.play(audioAcceptOther);
                    }
                }
                if (haveNotAccepted.length !== 0) {
                    return;
                }
                for (const [_userId, state] of this.userStates) {
                    const sum = Array.from(state.currentScores.values()).reduce(
                        (result, current) => result + current,
                        0,
                    );
                    state.score = (state.score ?? 0) + sum;
                }
                this.phase = Phase.SCORES;
            }),
        );
    }
}
