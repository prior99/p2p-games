export const enum Phase {
    GUESS = "guess",
    SCORING = "scoring",
    SCORES = "scores",
}