const baseConfig = require("../../webpack.config");

module.exports = (env, options) => ({ ...baseConfig(env, options, __dirname) });