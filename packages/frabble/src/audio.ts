import audioNextPhase from "../assets/sound-next-phase.mp3";

export {
    audioNextPhase,
};

export const allAudios = [
    { url: audioNextPhase, gain: 1.0 },
];
