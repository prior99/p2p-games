import * as React from "react";
import { external, inject } from "tsdi";
import { observer } from "mobx-react";
import { Phase } from "../../types";
import { Game } from "../../game";
import { unreachable, CommonPhase } from "p2p-games-common";
import { Lobby } from "../lobby";
import { GameContainer } from "../game-container";

@external
@observer
export class GameComponent extends React.Component {
    @inject private game!: Game;

    public render(): JSX.Element {
        switch (this.game.phase) {
            case CommonPhase.LOBBY:
                return <Lobby className="PageGame__lobby" />;
            case Phase.STARTED:
                return <GameContainer className="PageGame__gameContainer" />;
            default:
                unreachable(this.game.phase);
        }
    }
}
