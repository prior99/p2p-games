import * as React from "react";
import { external, inject } from "tsdi";
import { Form, Checkbox, StrictDropdownItemProps, Dropdown, DropdownProps, Input, Grid } from "semantic-ui-react";
import { computed, action } from "mobx";
import { observer } from "mobx-react";
import { Language } from "../../types";
import { Game } from "../../game";
import { getLanguages, getFlagIcon, getLanguageName } from "../../utils";
import { NetworkMode } from "p2p-networking";
import { Lobby as CommonLobby } from "p2p-games-common";
import { audioNextPhase } from "../../audio";

declare const SOFTWARE_VERSION: string;

export interface LobbyProps {
    className?: string;
}

@external
@observer
export class Lobby extends React.Component<LobbyProps> {
    @inject private game!: Game;

    @action.bound private handleLanguageChange(_: unknown, { value }: DropdownProps): void {
        this.game.config.language = value as Language;
    }

    @action.bound private handleTimeLimitChange(evt: React.SyntheticEvent<HTMLInputElement>): void {
        const value = evt.currentTarget.value;
        const parsed = Number(value);
        if (!value || isNaN(parsed)) {
            return;
        }
        this.game.config.timeLimit = parsed;
    }

    @action.bound private handleTimeLimitToggle(): void {
        if (this.game.config.timeLimit === undefined) {
            this.game.config.timeLimit = 60;
            return;
        }
        this.game.config.timeLimit = undefined;
    }

    @computed private get languageOptions(): StrictDropdownItemProps[] {
        return getLanguages().map((language) => ({
            key: language,
            value: language,
            flag: getFlagIcon(language),
            text: getLanguageName(language),
        }));
    }

    @computed private get language(): Language {
        return this.game.config.language;
    }

    @computed private get timeLimitEnabled(): boolean {
        return this.game.config.timeLimit !== undefined;
    }

    @computed private get timeLimit(): string {
        return String(this.game.config.timeLimit ?? "");
    }

    @computed private get isHost(): boolean {
        return this.game.networkMode === NetworkMode.HOST;
    }

    public render(): JSX.Element {
        return (
            <CommonLobby audio={audioNextPhase}>
                {this.isHost && (
                    <Grid.Row>
                        <Grid.Column>
                            <Form.Field>
                                <label>Language</label>
                                <Dropdown
                                    selection
                                    value={this.language}
                                    options={this.languageOptions}
                                    onChange={this.handleLanguageChange}
                                />
                            </Form.Field>
                            <Form.Field>
                                <Checkbox
                                    className="Lobby__toggle"
                                    checked={this.timeLimitEnabled}
                                    onChange={this.handleTimeLimitToggle}
                                    label="Use timer"
                                />
                            </Form.Field>
                            <Form.Field disabled={!this.timeLimitEnabled}>
                                <label>Time limit (seconds)</label>
                                <Input value={this.timeLimit} onChange={this.handleTimeLimitChange} />
                            </Form.Field>
                        </Grid.Column>
                    </Grid.Row>
                )}
            </CommonLobby>
        );
    }
}
