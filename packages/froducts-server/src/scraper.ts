import { getRandomProduct, Product } from "./utils";
import { info, error } from "winston";

export class Scraper {
    /**
     * A map of seeds and the respective loaded content.
     */
    private results = new Map<string, Product>();
    /**
     * Handlers waiting for the seeds to load.
     */
    private handlers = new Map<string, { resolve(result: Product): void; reject(reason: Error): void }[]>();

    /**
     * Load a specific seed.
     *
     * @param seed The seed to load.
     * @returns A promise resolving to the HTML.
     */
    public load(seed: string): Promise<Product> {
        return new Promise(async (resolve, reject) => {
            if (this.results.has(seed)) {
                info(`Using cached response for seed ${seed}.`);
                return resolve(this.results.get(seed));
            }
            // Seed was requested earlier. Attach handler to list of handlers.
            const seedHandlers = this.handlers.get(seed);
            if (seedHandlers) {
                seedHandlers.push({ resolve, reject });
                return;
            }
            // Seed was not requested yet. Start loading.
            this.handlers.set(seed, [{ resolve, reject }]);
            info(`Fetching new product for seed ${seed}.`);
            const result = await getRandomProduct(seed);
            if (!result) {
                error(`No result could be found for seed ${seed}.`);
                this.handlers.get(seed)?.forEach(({ reject: rejectHandler }) => rejectHandler(new Error("No result.")));
            } else {
                this.handlers.get(seed)?.forEach(({ resolve: resolveHandler }) => resolveHandler(result));
                this.results.set(seed, result);
                this.handlers.delete(seed);
            }
        });
    }
}
