import cheerio from "cheerio";
import { create as randomSeed, RandomSeed } from "random-seed";
import fetch from "node-fetch";
import { info, warn, error, debug } from "winston";

export interface Product {
    html: string;
    price: number;
}

const baseUrl = "https://www.amazon.de";
const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwyz1234567890";

const headers = {
    "User-Agent":
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36",
};

function randomSearchString(rng: RandomSeed): string {
    const result: string[] = [];
    const amount = rng.intBetween(3, 8);
    for (let i = 0; i < amount; ++i) {
        result.push(alphabet.charAt(rng.intBetween(0, alphabet.length - 1)));
    }
    return result.join("+");
}

async function getPageCount(url: string): Promise<number> {
    const req = await fetch(url, { headers });
    const $ = cheerio.load(await req.text());
    const buttonLabel = $(".a-pagination > li:nth-last-child(2)").text();
    const count = Number(buttonLabel);
    return isNaN(count) ? 1 : count;
}

async function getRandomPageUrl(searchString: string, rng: RandomSeed): Promise<string> {
    const max = await getPageCount(`${baseUrl}/s?k=${searchString}`);
    debug(`Search string ${searchString} has ${max} pages of results.`);
    return `${baseUrl}/s?k=${searchString}&page=${rng.intBetween(1, max)}`;
}

async function getRandomProductUrl(searchString: string, rng: RandomSeed): Promise<string | undefined> {
    const req = await fetch(await getRandomPageUrl(searchString, rng), { headers });
    const $ = cheerio.load(await req.text());
    const results = $(".s-search-results > div[data-component-type=s-search-result] h2 a").get();
    const result = results[rng.intBetween(0, results.length)];
    const url = $(result).attr("href");
    if (!url) {
        error(`Search string ${searchString} didn't have any products.`);
    }
    debug(`Search string ${searchString} lead to product url: ${url}`);
    return url ? `${baseUrl}${url}` : undefined;
}

export async function getRandomProduct(seed: string, retry = 0): Promise<Product | undefined> {
    info(`Fetching product for seed ${seed}. Attempt (${retry + 1}/10)`);
    const rng = randomSeed(seed + retry);
    const searchString = randomSearchString(rng);
    const url = await getRandomProductUrl(searchString, rng);
    const attemptAgain = async (): Promise<Product | undefined> => {
        if (retry < 10) {
            return await getRandomProduct(seed, retry + 1);
        } else {
            error(`Too many failed attempts for seed ${seed}. Giving up.`);
        }
    };
    if (url) {
        const text = await (await fetch(url, { headers })).text();
        const $ = cheerio.load(text);
        $("head").append(`
            <style>
                .a-color-price {
                    color: rgba(0, 0, 0, 0) !important;
                    background-color: #B12704!important;
                }
            </style>
        `);
        $(".navLeftFooter").remove();
        $("a").attr("href", "#");
        $("script").html("");
        $("header").remove();
        const regex = $("#rightCol .a-color-price")
            .html()
            ?.match(/(\d+,?\d*)/);
        if (!regex) {
            warn(`Product for seed ${seed} had no price.`);
            return await attemptAgain();
        }
        const price = regex[1]?.trim().replace(",", ".");
        if (!price || isNaN(Number(price))) {
            warn(`Product for seed ${seed} had a price that was not a number: ${price}.`);
            return await attemptAgain();
        }
        info(`Product found for seed ${seed}.`)
        return { price: Number(price), html: $.html().replace(/\d+,?\d*&#xA0;&#x20AC;/g, "XX,XX&#xA0;&#x20AC;") };
    }
    return await attemptAgain();
}
