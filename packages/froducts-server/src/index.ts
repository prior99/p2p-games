import Express from "express";
import morgan from "morgan";
import { Scraper } from "./scraper";
import * as Winston from "winston";
import { info, error } from "winston";

Winston.remove(Winston.transports.Console);
Winston.add(
    new Winston.transports.Console({
        format: Winston.format.combine(
            Winston.format.timestamp({ format: "YYYY-MM-DDTHH:mm:ss" }),
            Winston.format.colorize(),
            Winston.format.printf((info) => `${info.timestamp} - ${info.level}: ${info.message}`),
        ),
        level: "verbose",
    }),
);

const app = Express();
const scraper = new Scraper();

app.use(morgan("tiny", { stream: { write: (msg) => info(msg.trim()) } }));
app.use((req, res, next) => {
    if (req.headers.origin === "http://localhost:8080") {
        res.header("Access-Control-Allow-Origin", "http://localhost:8080");
    } else {
        res.header("Access-Control-Allow-Origin", "https://prior99.gitlab.io");
    }
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    if (req.method === "OPTIONS") {
        res.sendStatus(200);
        return;
    }
    return next();
});

app.get("/froducts/:seed", async (req, res) => {
    const seed: string = req.params.seed;
    try {
        const result = await scraper.load(seed);
        res.status(200).json(result);
    } catch (err) {
        error(err.message);
        res.status(500).json({ error: "No result." });
    }
});

app.listen(9009);
